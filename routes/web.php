<?php

use Illuminate\Support\Facades\Route;
use Silber\Bouncer\Database\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('customer', 'CustomersController@index')->name('customer');
Route::get('customerslist', 'CustomersController@customerslist')->name('customerslist');
Route::get('product', 'ProductsController@index')->name('product');
Route::get('productslist', 'ProductsController@productslist')->name('productslist');
Route::get('order', 'OrdersController@index')->name('product');
Route::get('orderslist', 'OrdersController@orderslist')->name('orderslist');
Route::get('orderDetail/{id}', 'OrdersController@detail')->name('order');
