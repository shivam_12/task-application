<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id'=>$faker->numberBetween(1,50),
        'product_id' => $faker->numberBetween(1,100),
        'quanty' => $faker->numberBetween(1,5),

    ];
});
