<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'customer_id'=>$faker->numberBetween(1,200),
        'invoice_number' => $faker->unique()->randomNumber($nbDigits = 8),
        'total_amount' => $faker->numberBetween($min = 1500, $max = 6000),
        'status' => $faker->randomElement($array = array ('new','processed')),
    ];
});
