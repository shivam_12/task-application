<?php

use App\OrderItem;
use Facade\Ignition\Middleware\CustomizeGrouping;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Codec\OrderedTimeCodec;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(OrderItemSeeder::class);
    }
}
