<?php

use App\Customer;
use App\Order;
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
// use Bouncer;
use Silber\Bouncer\BouncerFacade as Bouncer;

// use Silber\Bouncer\Bouncer;

class UserSeeder extends Seeder
{
    // use Bouncer;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow('administrator')->toOwnEverything();
        Bouncer::allow('user-manager')->toManage(Customer::class);
        Bouncer::allow('shop-manager')->toManage([Product::class,Order::class]);

        $administrator = factory(App\User::class)->create([
            'name' => 'administrator',
            'email' => 'administrator@gmail.com',
            'password' => Hash::make('password'),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $administrator->assign('administrator');


        $user_manager = factory(App\User::class)->create([
            'name' => 'user-manager',
            'email' => 'user-manager@gmail.com',
            'password' => Hash::make('password'),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $user_manager->assign('user-manager');

        $shop_manager = factory(App\User::class)->create([
            'name' => 'shop-manager',
            'email' => 'shop-manager@gmail.com',
            'password' => Hash::make('password'),
            'created_at' =>Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' =>Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        $shop_manager->assign('shop-manager');

    }
}
