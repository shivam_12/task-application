@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table id="myTable" class="table table-bordered" >
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Total Amount</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

         $(function() {
            $.noConflict();
               $('.table-bordered').DataTable({
               processing: true,
               serverSide: true,
               ajax: "{{ route('orderslist') }}",
               columns: [
                        { data: 'id', name: 'id' },
                        {data: 'name', name: 'name'},
                        { data: 'total_amount', name: 'total_amount' },
                        { data: 'status', name: 'status', searchable: false},
                        { data: 'action', name: 'action'}

                     ]
            });
         });

         </script>
@endsection
