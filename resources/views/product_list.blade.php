@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table id="myTable" class="table table-bordered" >
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Stock Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

         $(function() {
            $.noConflict();
               $('.table-bordered').DataTable({
                dom: 'Bfrtip',
        buttons: [
            'columnsToggle'
        ], 
               processing: true,
               serverSide: true,
               ajax: "{{ route('productslist') }}",
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'price', name: 'price' },
                        { data: 'stock', name: 'stock'}

                     ]
            });
    //         $('a.toggle-vis').on( 'click', function (e) {
    //     e.preventDefault();
 
    //     // Get the column API object
    //     var column = table.column( $(this).attr('data-column') );
 
    //     // Toggle the visibility
    //     column.visible( ! column.visible() );
    // } );
         });

         </script>
@endsection
