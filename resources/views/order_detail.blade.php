@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                  @if(isset($orderDetail))
                       
                        <table>
                            <tr>
                                <th>Invoice No:</th>
                                <td>{{$orderDetail[0]->order->invoice_number}}</td>
                            </tr>
                            <tr>
                                 <th>Amount</th> 
                                <td>{{$orderDetail[0]->order->total_amount}}
                                </td> 
                            </tr>    
                            <tr>
                                <th> Product name: </th><td>{{$orderDetail[0]->product->name}}
                                </td>
                            </tr> 
                            <tr>
                                <th>Customer Name:</th> <td>{{$orderDetail[0]->order->customer->name}}
                                </td>
                            </tr>
                             <tr>
                                <th>order date:</th> <td>{{$orderDetail[0]->order->created_at}}
                                </td>
                            </tr>
                             <tr>
                             <th>order Status: </th><td>{{$orderDetail[0]->order->status}}
                            </td>
                            </tr> 
                            
                        </table>
                  @else
                    <span>No Data found</span>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
