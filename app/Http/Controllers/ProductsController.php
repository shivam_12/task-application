<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Yajra\DataTables\DataTables;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('product_list');
    }
    public function productslist(){
        if(Bouncer::is(Auth::user())->a('administrator','shop-manager')){
            $product=Product::query();
            return DataTables::of($product)
            ->addColumn('stock',function ($product){
                if($product->in_stock==0){
                    return 'Out of Stock';
                }
                else{
                    return 'In Stock';
                }

            })
            ->make(true);
        } 
    }
}
