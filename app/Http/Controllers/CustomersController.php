<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Illuminate\Support\Carbon;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        
        return view('customer_list');
    }
    public function customerslist(){
        if(Bouncer::is(Auth::user())->a('administrator','user-manager')){
            $data=Customer::query();
            return DataTables::of($data)
            ->addColumn('created_at', function ($data) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/m/y');
            })->make(true);
        } 
    }

}
