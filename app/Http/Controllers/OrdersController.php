<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Yajra\DataTables\DataTables;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order_list');
    }
    public function orderslist(){
        if(Bouncer::is(Auth::user())->a('administrator','shop-manager')){
            $data=Order::query()->with(['customer']);
            return DataTables::of($data)
                            ->addColumn('name', function($data)
                            {
                               return $data->customer->name;
                            })
                            ->addColumn('action', function ($data) {
                                return '<a href="/orderDetail/'.$data->id.'" class="btn btn-xs btn-primary"><i class=""></i> view</a> ';})
                            ->make(true);

        }
    }

    public function detail($id){
        $orderDetail=OrderItem::with(['order','product'])->where('order_id',$id)->get();
        return view('order_detail',compact('orderDetail'));

    }
}
